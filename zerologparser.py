#!/usr/bin/env python
import struct
import time
import math
import re
import parsers

from utils import *

class DataObject(object):
	data = ""

	def toHex(self):
		return "".join(map(hexOfByte, self.data))

	def toPrintable(self):
		return printable(self.data)

	def getLocation(self):
		return str(self.start) + "-" + str(self.start + self.length)

	def toHexDump(self):
		return self.getLocation() + " : " + self.toHex()

	def toPrintableDump(self):
		return self.getLocation() + " : " + self.toPrintable()


class DataRecord(DataObject):
	def __init__(self, data, start):
		self.data = data
		self.start = start
		self.length = len(data)
		self.indicatedLength = ord(data[0])

		if self.indicatedLength != self.length:
			self.validRecord = False
		else:
			self.validRecord = True

	def suggestedRecordLen(self):
		if self.validRecord:
			return self.length
		else:
			return findValidSubRecordLength(self.data)

class DataBlock(DataObject):
	def __init__(self, data, start, end=None):
		self.start = start
		if end:
			self.length = end - start
		else:
			self.length = len(data) - start
		self.data = data[start:end]

	def recordGenerator(self):
		''' yields the data records, split on \xB2, because the re.findall approach will skip trailing data'''
		s = 0
		i = 1
		while i > 0:
			i = self.data.find('\xB2', s)
			if i >= 0:
				yield self.data[s:i+1]
				s = i+1
			elif s < len(self.data): 
				yield self.data[s:len(self.data)]

	def getRecords(self):
		records = []

		offset = self.start
		#for r in re.findall('[^\xB2]*\xB2', self.data):
		for r in self.recordGenerator():
			records.append(DataRecord(r, offset))
			offset += len(r)
		return records

	def dumpHexRecords(self, outFn):
		for r in self.getRecords():
			outFn(r.toHexDump())

	def dumpPrintableRecords(self, outFn):
		for r in self.getRecords():
			outFn(r.toPrintableDump())

class MasterParser(object):
	def __init__(self, filename, output):
		self.activeParser = None

		self.filename = filename
		self._output = output
		self.lastByte = None
		self.expect = None

		self.input = open(filename, "rb").read()
		self.input_length = len(self.input)
		self.idx = -1
		self.blocksSetup = False
		
	def parseFile(self):
		while self.has_next():
			byte = self.next()
			self.parse(byte)

		self._output.write("\n")

	def dumpRecords(self, block):
		""" TODO: This should dump the file in some way that helps analyze it. """
		self.setupBlocks()

		for r in block.getRecords():
			location = str(r.start) + "-" + str(r.start + r.length)
			self.outln(location + " : " + r.toHex() + (r.validRecord and " " or "  *******************************INVALID******************************** " + str(r.suggestedRecordLen())))

	def analyzeHeader(self):
		self.setupBlocks()

		self.out(self.header1.toHex() + '\n\n')
		self.out(self.header1.toPrintable() + '\n\n')
		self.out(self.header2.toHex() + '\n\n')
		self.out(self.header2.toPrintable() + '\n\n')

		self.out(repr(self.decodeHeader2()) + '\n\n')

		self.out(self.header3.toHex() + '\n\n')
		self.out(self.header3.toPrintable() + '\n\n')

		self.out(repr(self.decodeHeader3()) + '\n\n')

		self.out(self.header4.toHex() + '\n\n')
		self.out(self.header4.toPrintable() + '\n\n')

		self.decodeHeader4()

	def setupBlocks(self):
		if self.blocksSetup == False:
			self.header1 = DataBlock(self.input, 0, self.input.index(bytesFor("A1A1A1A1"))+4)
			self.header2 = DataBlock(self.input, self.input.index(bytesFor("A1A1A1A1"))+4, self.input.index(bytesFor("A0A0A0A0"))+4)
			self.header3 = DataBlock(self.input, self.input.index(bytesFor("A0A0A0A0"))+4, self.input.index(bytesFor("A3A3A3A3"))+4)
			self.header4 = DataBlock(self.input, self.input.index(bytesFor("A3A3A3A3"))+4, self.input.index(bytesFor("A2A2A2A2"))+4)
			self.body	 = DataBlock(self.input, self.input.index(bytesFor("A2A2A2A2"))+4) # 1540 Byte offset

			self.blocksSetup = True

	def extractBlock(self, start, end):
		b = {}

	def decodeHeader2(self):
		self.setupBlocks()
		d = {}
		d['model'] = ""
		d['built'] = self.header2.data[502:522]
		d['vin'] = self.header2.data[534:551]
		d['serial'] = self.header2.data[470:491]
		d['runtime'] = ""
		d['testmode'] = ""
		return d

	def decodeHeader3(self):
		self.setupBlocks()
		d = {}
		d['model'] = getCString(self.header3.data, 36)
		return d

	def decodeHeader4(self):
		self.setupBlocks()
		self.dumpRecords(self.header4)

	def parse(self, block):
		for r in block.getRecords():
			P = parsers.getMatchingParser(r)
			if P:
				p = P(r)
				self.outln(p.getResults(debug))
			else:
				self.outln(r.toHexDump() + "  NO PARSER FOUND")

	def dumpRecordWidthLine(self):
		for i in range(80,0,-1):
			self.out(hexOfByte(chr(i)))
		self.out("B2\n")

	def out(self, output):
		self._output.write(output)

	def outln(self, output):
		self._output.write(output + '\n')


if __name__ == "__main__":
	import argparse
	import sys

	parser = argparse.ArgumentParser()
	parser.add_argument("logfile")
	parser.add_argument("-o", "--output-file", type=str, help="file to write output to (defaults to stdout)")
	parser.add_argument("-d", "--dump-records", action="store_true", help="dump the bytes in raw-ish format after splitting on record boundaries and translating to hexadecimal")
	parser.add_argument("--dump-printable-records", action="store_true", help="dump the bytes in raw-ish format after splitting on record boundaries and translating to hexadecimal")
	parser.add_argument("-a", "--analyze-header", action="store_true", help="print the header info out a couple different ways to help identify patterns")
	parser.add_argument("-t", "--run-tests", action="store_true", help="run some tests on the script parser.")
	parser.add_argument("-p", "--process-log", action="store_true", help="process the log file")
	parser.add_argument("-v", "--verbose", action="store_true", help="process the log file, outputting decorated versions of the raw bytes")
	args = parser.parse_args()

	if args.output_file:
		out = file(args.output_file, "w")
	else:
		out = sys.stdout

	f = MasterParser(args.logfile, out)

	didSomething = False

	if args.analyze_header:
		didSomething = True
		f.analyzeHeader()

	if args.dump_printable_records:
		didSomething = True
		f.setupBlocks()

		f.header4.dumpPrintableRecords(f.out)
		f.out('\n\n')
		f.body.dumpPrintableRecords(f.out)

	if args.dump_records:
		didSomething = True
		f.setupBlocks()
		f.dumpRecords(f.header1)
		f.dumpRecords(f.header2)
		f.dumpRecords(f.header3)
		f.dumpRecords(f.header4)
		f.dumpRecords(f.body)

	if args.run_tests:
		didSomething = True
		bytes = map(chr, [0X07,0X00,0X06,0X0C,0X0A,0X0B,0X00,0xB2])
		f.outln("findValidSubRecordLength('" + hexOfBytes(bytes) + "') = " + str(findValidSubRecordLength(bytes)))

		bytes = map(chr, [0X07,0X00,0X07,0X0C,0X0A,0X0B,0X00,0xB2])
		f.outln("findValidSubRecordLength('" + hexOfBytes(bytes) + "') = " + str(findValidSubRecordLength(bytes)))

		f.outln(bytesFor("50"))
		f.outln(bytesFor("4f"))

		try:
			f.outln(bytesFor("4g"))
		except ValueError, e:
			f.outln(str(e))

		try:
			f.out(bytesFor("4ga"))  #crash
		except Exception, e:
			f.outln(str(e))
	
	if not didSomething:
		f.setupBlocks()
		if args.verbose:
			debug = True
		else:
			debug = False

		f.parse(f.header4)

		f.parse(f.body)
