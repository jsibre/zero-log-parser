import string
import time

myprintable = string.digits + string.letters + string.punctuation + '\t '

def hexOfByte(byte, width=2):
	if byte != None:
		return hexOfWidth(ord(byte), width, False)
	else:
		return None

def hexOfBytes(bytes):
	return "".join(map(hexOfByte, bytes))

def hexOfWidth(integer, width, with0x=True):
	hx = hex(integer)
	p = hx[:hx.index('x')+1]
	v = hx[hx.index('x')+1:]
	v = ("000000000000000000000000000000000000000000000000" + v.upper())[-width:]

	if with0x:
		return p + v
	else:
		return v

def bytesFor(byteString):
	if len(byteString) % 2 != 0:
		raise ValueError("Byte String must be n pairs of hexadecimal digits (0-F).  This one had an odd number of characters.")
	byteString = byteString.upper()
	for b in byteString:
		if b not in "0123456789ABCDEF":
			raise ValueError("Byte String must be n pairs of hexadecimal digits (0-F).  This one had a '" + b + "' in it.")
	return byteString.decode("hex")

def printable(bytes):
	def printABLE(b):
		if b in myprintable:
			return b
		else:
			return "."
	return ''.join(map(printABLE, bytes))

def getCString(bytes, start):
	r = []
	for i in range(start, len(bytes)):
		if bytes[i] != chr(0):
			r.append(bytes[i])
		else:
			break
	return ''.join(r)
	

def findValidSubRecordLength(r):
	''' This method searches a record to figure out if there's a valid subsection in it.
	It assumes the record ends with B2, and looks to see if, somewhere inside, there's a 
	byte that correctly represents the length from that byte's location to the end of the 
	record. If it finds one, it returns that length (which means the last 'n' bytes are a 
	valid record.)  Otherwise, returns None. '''
	l = len(r)
	if l > 0 and r[l-1] == '\xB2':
		for i in range(0, min(255,l)):
			if chr(l-i) == r[i]:
				return l-i
	return None

def binary(value, places, reverse=False):
	retVal = []
	for i in range(places):
		placeValue = 2 ** i
		if value & placeValue:
			retVal.append("1")
		else:
			retVal.append("0")
	if not reverse:
		# Since we're appending to build the list, vs prepending, we have to reverse 
		# to get the natural order of 8421
		retVal.reverse()
	return ''.join(retVal)

def yesOrNo(value):
	if value:
		return "Yes"
	else:
		return "No"

def onOrOff(value):
	if value:
		return "On"
	else:
		return "Off"

def convertIntToTime(i):
	#This is specific to the Zero logs - it adjusts based on PT
	if i == 0:
		return None
	else:
		return time.gmtime(i-(3600 * 7))

