#!/usr/bin/env python
import string
import struct
import time
import math
import re

def hexOfByte(byte, width=2):
	if byte != None:
		return hexOfWidth(ord(byte), width, False)
	else:
		return None

def hexOfBytes(bytes):
	return "".join(map(hexOfByte, bytes))

def hexOfWidth(integer, width, with0x=True):
	hx = hex(integer)
	p = hx[:hx.index('x')+1]
	v = hx[hx.index('x')+1:]
	v = ("000000000000000000000000000000000000000000000000" + v.upper())[-width:]

	if with0x:
		return p + v
	else:
		return v


def binary(value, places, reverse=False):
	retVal = []
	for i in range(places):
		placeValue = 2 ** i
		if value & placeValue:
			retVal.append("1")
		else:
			retVal.append("0")
	if not reverse:
		# Since we're appending to build the list, vs prepending, we have to reverse 
		# to get the natural order of 8421
		retVal.reverse()
	return ''.join(retVal)

def yesOrNo(value):
	if value:
		return "Yes"
	else:
		return "No"
	
class BaseParser(object):
	matchList = []

	def matches(self, signature):
		if signature in self.matchList:
			print "Matched '" + signature + "' in Parser"
			return True
		

class FlagParser(BaseParser):
	matchDict = {
		"1E":"", 
		"1F":"CAN ACK error", 
		"20":"CAN error", 
		"21":"Enabling Charger", 
		"22":"Disabling Charger",
		"23":"Disabling Charger",
		"25":"",
		"24":"",
		"28":"Reset: Power-On",
		"29":"Sevcon Contactor Drive change",
		"2E":"Contactor Open",
		"2F":"Contactor Open",
		"30":"Contactor Closing",
		"31":"Contactor Closed",
		"36":"",
		"3D":"Module mode Change",
		"43":"Module scheme changed",
		"44":"Module scheme changed",
		"46":"Module scheme changed",
		"47":"Module scheme changed"
		}

	def matches(self, signature):
		if signature in self.matchDict.keys():
			#print "Matched '" + signature + "' in Parser"
			return True
		#print "Did not match '" + signature + "' in Parser"

	def parse(self, byte):
		return " Flag: <" + hexOfByte(byte) + "> \"" + self.matchDict[hexOfByte(byte)] +"\""

class IntParser(object):
	def __init__(self):
		self.buffer = []

	def parse(self, byte):
		self.buffer.append(byte)
		if len(self.buffer) == 4:
			i= struct.unpack("i","".join(self.buffer))[0]
			return i

class TimeParser(object):
	def __init__(self):
		self.intParser = IntParser()
		self.buffer = ""

	def parse(self, byte):
		self.buffer = hexOfByte(byte) + " " + self.buffer
		return self.process(self.intParser.parse(byte))

	def process(self, i):
		if i != None:
			t = time.localtime(i)
			f = time.strftime("%Y-%m-%d %H:%M:%S", t)
			return " [" + f + "] "  #+" (" + self.buffer + " -- " + str(i) + ")"
			#return " [" + self.buffer + "]" + " [" + f + "] "  #+" (" + self.buffer + " -- " + str(i) + ")"



class MessageParser(object):
	NUL = chr(0)
	RECORD_TERMINATOR = chr(int("b2", 16))
	foundNUL = False

	def __init__(self):
		self.buffer = []

	def parse(self, byte):
		if self.foundNUL and byte == self.RECORD_TERMINATOR:
			return "".join(self.buffer)  + "<msg>"
		elif byte == self.NUL:
			self.foundNUL = True
		else:
			self.buffer.append(byte)

class ByteBuffer(object):
	def __init__(self, expectedLength):
		self.expectedLength = expectedLength
		self.buffer = []
		self.outputBuffer = []

	def parse(self, byte):
		self.buffer.append(byte)
		self.outputBuffer.append(hexOfByte(byte))
		if len(self.buffer) == self.expectedLength:
			return "".join(self.outputBuffer)

	def getBytes(self, length=None):
		if length == None:
			length = len(self.buffer)
		return "".join(self.buffer[:length])


class StatusRecordParser(object):
	struct = None
	structPattern = None
	structKeys = None
	label = None
	length = None
	debug = True

	def __init__(self, header, length=None, label=None, structPattern=None, structKeys=None):

		self.header = header
		if length != None:
			self.length=length
		if label != None:
			self.label = label
		if structPattern != None:
			self.structPattern = structPattern
		if structKeys != None:
			self.structKeys = structKeys

		self.time = None
		self.timeParser = TimeParser()
		self.bytes = None
		self.byteBuffer = ByteBuffer(self.length)
		self._result = None

		if self.structPattern:
			self.struct = struct.Struct(self.structPattern)

	def parse(self, byte):
		if self.time == None:
			self.time = self.timeParser.parse(byte)
		elif self.bytes == None:
			self.bytes = self.byteBuffer.parse(byte)
		if self.time and self.bytes:
			if self.struct:
				self.unpack()
			r = self.parseResults()
			if self.debug:
				return "%s%s%s%-26s %s" % (self.header, self.time, self.decoratedOutputBuffer(), self.label or "", r)
			else:
				return "%s%-26s %s" % (self.time, self.label or "", r)

	def parseResults(self):
		return ""

	def decoratedOutputBuffer(self):
		if not self.struct:
			return self.bytes
		else:
			start = 0
			output = ""
			ob = "".join(self.byteBuffer.outputBuffer)
			fieldWidth = ""
			for c in self.struct.format:
				d=""
				if c == "=":
					l=0
				elif c in "x":
					l=1
					d="x"
				elif c in "Bbc?s":
					l=1
				elif c in "hH":
					l=2
				elif c in "iIlLf":
					l=4
				elif c in "dqQ":
					l=8
				elif c in "0123456789":
					l=0
					fieldWidth += c
				else:
					return "UNRECOGNIZED FIELD SPECIFIER - " + c
				
				if l != 0 and fieldWidth != "":
					l = int(fieldWidth)
					fieldWidth = ""

				if l:
					output += d + ob[start:start+(l*2)] + d
					output += " "
					start = start + (l*2)
			output += ob[start:]
			return output

	def unpack(self):
		self._result = self.struct.unpack(self.byteBuffer.getBytes(struct.calcsize(self.struct.format)))

	def getStructValue(self, key):
		return self._result[self.structKeys.index(key)]

	def dump(self):
		fs = "%6s " * len(self._result)
		return fs % self._result

class EmergencyFrameRecordParser(StatusRecordParser):
	length=9
	label="SEVCON CAN EMCY Frame"
	structPattern="HhbBBB"
	structKeys="errCode,sevErrCode,errReg,d1,d2,d3".split(",")
	debug = True

	sevConErrCodes = {
		"0x4603":"Motor in thermal c/b",
		"0x4884":"Sequence Fault",
		"0x5101":"Line Contactor o/c",
		"0x5319":"Mtr Slv in Wrong State"
	}


	def parseResults(self):
		return "Error Code: %s, Error Reg: %s, Sevcon Error Code: %s, Data: %s %s %s, %s" % (
				hexOfWidth(self.getStructValue("errCode"), 4), 
				hexOfWidth(self.getStructValue("errReg"),2), 
				hexOfWidth(self.getStructValue("sevErrCode"),4), 
				hexOfWidth(self.getStructValue("d1"),2), 
				hexOfWidth(self.getStructValue("d2"),2), 
				hexOfWidth(self.getStructValue("d3"),2),
				self.sevConErrCodes.get(hexOfWidth(self.getStructValue("sevErrCode"),4), "Unrecognized Sevcon Error Code **********")
				)


class RidingRecordParser(StatusRecordParser):
	length=28
	label="Riding"
	structPattern="=bbbxihhiBxBBxbxH"
	structKeys="packTH,packTL,packSOC,vPack,motTemp,ctrlTemp,motRPM,battAmps,mods,motAmps,ambTemp,odo".split(",")
	debug = False

	def parseResults(self):
		return "PackTemp: h%3sC, l%3sC, PackSOC:%3s%%, Vpack:%7.3fV, MotAmps: %3s, BattAmps: %3s, Mods: %s, MotTemp: %3sC, CtrlTemp: %3sC, AmbTemp: %3sC, MotRPM:%4s, Odo: %skm" % ( 
			self.getStructValue("packTH"),
			self.getStructValue("packTL"),
			self.getStructValue("packSOC"),
			self.getStructValue("vPack")/1000.0,
			self.getStructValue("motAmps"),
			self.getStructValue("battAmps"),
			binary(self.getStructValue("mods"), 2, True),
			self.getStructValue("motTemp"),
			self.getStructValue("ctrlTemp"),
			self.getStructValue("ambTemp"),
			self.getStructValue("motRPM"),
			self.getStructValue("odo")
			)

class ChargingRecordParser(StatusRecordParser):
	length=16
	label="Charging"
	structPattern="=bbbxihhbbb"
	structKeys="packTH,packTL,packSOC,packV,battAmps,MbbChgEn,Mods,AmbTemp,BmsChgEn".split(",")
	debug = False

	def parseResults(self):
		return "PackTemp: h%3sC, l%3sC, AmbTemp: %sC, PackSOC:%3s%%, Vpack:%6.3fV, BattAmps: %3s, Mods: %s, MbbChgEn: %s, BmsChgEn: %s" % ( 
			self.getStructValue("packTH"),
			self.getStructValue("packTL"),
			self.getStructValue("AmbTemp"),
			self.getStructValue("packSOC"),
			self.getStructValue("packV")/1000.0,
			self.getStructValue("battAmps"),
			binary(self.getStructValue("Mods"), 2, True),
			yesOrNo(self.getStructValue("MbbChgEn")),
			yesOrNo(self.getStructValue("BmsChgEn"))
			)

class LowChassisIsolationParser(StatusRecordParser):
	length=6
	label="Low Chassis Isolation"
	structPattern="=bxxxb"
	structKeys="isolation,cell".split(",")

	def parseResults(self):
		#return repr(self._result)
		return "%s KOhms to cell %s" % ( 
			self.getStructValue("isolation"),
			self.getStructValue("cell")
			)

class BMSIsolationFaultParser(StatusRecordParser):
	length=2
	label="BMS Isolation Fault"
	structPattern="=b"
	structKeys="pack".split(",")

	def parseResults(self):
		#return repr(self._result)
		return "BMS: %s" % ( 
			self.getStructValue("pack"),
			)

class ContactorChangeParser(StatusRecordParser):
	length=24
	label="Contactor Change"
	structPattern="=BBiiiihh"
	structKeys="closing,module,vmod,maxsys,minsys,vcap,battCurr".split(",")
	debug=False

	def parseResults(self):
		if self.getStructValue("closing"):
			action = "Closing"
		else:
			action = "Opening"

		self.label = "Module %s %s Contactor" % (
			self.getStructValue("module"),
			action
			)


		if self.getStructValue("closing"):
			diff = (self.getStructValue("maxsys")/1000.0) - (self.getStructValue("minsys")/1000.0)
			prechg = math.floor((self.getStructValue("vcap")/1000.0) / (self.getStructValue("vmod")/1000.0) * 100)
			return "vmod:%8.3fV, maxsys:%8.3fV, minsys:%8.3fV, diff:%6.3fV, vcap:%8.3fV, prechg: %i%%" % ( 
				self.getStructValue("vmod")/1000.0,
				self.getStructValue("maxsys")/1000.0,
				self.getStructValue("minsys")/1000.0,
				diff,
				self.getStructValue("vcap")/1000.0,
				prechg
				)
		else:
			return "vmod:%8.3fV, batt curr: %4sA" % ( 
				self.getStructValue("vmod")/1000.0,
				self.getStructValue("battCurr")
				)

class CANLinkUpParser(StatusRecordParser):
	length=2
	label=""
	structPattern="=b"
	structKeys="module".split(",")
	debug = False

	def parseResults(self):
		self.label = "Module %02i CAN Link Up" % ( 
			self.getStructValue("module"),
			)
		return ""

class CANLinkDownParser(StatusRecordParser):
	length=2
	label=""
	structPattern="=b"
	structKeys="module".split(",")
	debug = False

	def parseResults(self):
		self.label = "Module %02i CAN Link Down" % ( 
			self.getStructValue("module"),
			)
		return ""


class SevconPowerParser(StatusRecordParser):
	length=2
	label=""
	structPattern="=b"
	structKeys="power".split(",")
	debug = False

	def parseResults(self):
		self.label = "Sevcon Turned %s" % ( 
			(self.getStructValue("power") and "On" or "Off") ,
			)
		return ""


class RegistrationParser(StatusRecordParser):
	length=30
	label=""
	structPattern="=xBi23s"
	structKeys="module,vmod,serial".split(",")
	debug = False

	def parseResults(self):
		#return repr(self._result)
		return "Module %02i Registered     serial: %s,  vmod:%8.3f" % ( 
			self.getStructValue("module") ,
			self.getStructValue("serial").strip(),
			self.getStructValue("vmod")/1000.0
			)



class LogRecordParser(object):
	def __init__(self, header):
		self.header = header
		self.time = None
		self.timeParser = TimeParser()
		self.message = None
		self.messageParser = MessageParser()

	def parse(self, byte):
		if self.time == None:
			self.time = self.timeParser.parse(byte)
		elif self.message == None:
			self.message = self.messageParser.parse(byte)
			if self.message != None:
				return "%s%s" % (self.time, self.message)


class MasterParser(object):
	printable = string.digits + string.letters + ".,:;!? "
		
	def __init__(self, filename, output):
		self.activeParser = None

		self.filename = filename
		self._output = output
		self.lastByte = None
		self.expect = None

		self.input = open(filename, "rb").read()
		self.input_length = len(self.input)
		self.idx = -1

	def parseFile(self):
		while self.has_next():
			byte = self.next()
			self.parse(byte)

		self._output.write("\n")

	def dumpRecords(self):
		header = self.input[:1540]
		body = self.input[1540:]

		records = re.findall('[^\xB2]*\xB2', body)

		self.out(hexOfBytes(header) +"\n\n\n")
		bytes = len(header)
		for r in records:
			indicatedLength = ord(r[0])
			if indicatedLength != len(r):
				validRecord = False
				suggestedRecordLen = self.findValidSubRecordLength(r)
			else:
				validRecord = True

			location = str(bytes) + "-" + str(bytes + len(r))
			self.out(location + " : " + hexOfBytes(r) + (validRecord and " " or "  *******************************INVALID******************************** " + str(suggestedRecordLen)) + "\n")
			bytes += len(r)

	def findValidSubRecordLength(self, r):
		''' This methoed searches a record to figure out if there's a valid subsection in it.
		It assumes the record ends with B2, and looks to see if, somewhere inside, there's a 
		byte that correctly represents the length from that byte's location to the end of the 
		record. If it finds one, it returns that length (which means the last 'n' bytes are a 
		valid record.)  Otherwise, returns None. '''
		l = len(r)
		for i in range(0, l):
			if chr(l-i) == r[i]:
				return l-i
		return None

	def dumpRecordWidthLine(self):
		for i in range(80,0,-1):
			self.out(hexOfByte(chr(i)))
		self.out("B2\n")

	def has_next(self):
		if self.idx < self.input_length-1:
			return True
		else:
			return False

	def next(self):
		self.idx += 1
		return self.read(self.idx)

	def peek(self, i):
		return self.read(self.idx + i)

	def read(self, idx):
		if idx < self.input_length:
			return self.input[idx]
		else: 
			return None

	def out(self, output):
		self._output.write(output)

	def getMatchingParser(self, c):
		if c == "FD":
			return LogRecordParser(c)
		elif c == "2A": #["1F", "20", "21", "22", "28", "29", "2E", "2F", "30", "3D", "43", "44", "46"]:
			return StatusRecordParser(c, 1)
		else:
			n = c+hexOfByte(self.peek(1))
			if n in ["073B"]:
				self.idx += 1
				return StatusRecordParser(n, 1)
			if n in ["072A", "072B", "073B", "0801", "0809", "0909", "0936"]:
				self.idx += 1
				return StatusRecordParser(n, 2)
			elif n == "0828": #CAN Link Up
				self.idx += 1
				return CANLinkUpParser(n)
			elif n == "0829": #CAN Link Down
				self.idx += 1
				return CANLinkDownParser(n)
			elif n == "0831": #BMS Isolation Fault 
				self.idx += 1
				return BMSIsolationFaultParser(n)
			elif n == "0836": #Sevcon power Status
				self.idx += 1
				return SevconPowerParser(n)
			elif n == "0934" or n=="0930":
				self.idx += 1
				return StatusRecordParser(n, 3)
			elif n == "0B3D":
				self.idx += 1
				return StatusRecordParser(n, 4)
			elif n == "0C3A": #Low Chassis Isolation
				self.idx += 1
				return LowChassisIsolationParser(n)
			elif n == "0D26":
				self.idx += 1
				return StatusRecordParser(n, 7)
			elif n == "0F39":
				self.idx += 1
				return StatusRecordParser(n, 9)
			elif n == "1039":
				self.idx += 1
				return StatusRecordParser(n, 10)
			elif n == "0F2F":
				self.idx += 1
				return EmergencyFrameRecordParser(n)
			elif n == "162D":
				self.idx += 1
				return ChargingRecordParser(n)
			elif n == "172D":
				self.idx += 1
				return StatusRecordParser(n, 17)
			elif n == "1E33":
				self.idx += 1
				#return StatusRecordParser(n, 24)
				return ContactorChangeParser(n)
			elif n == "1F33":
				self.idx += 1
				return StatusRecordParser(n, 25)
			elif n == "222C":
				self.idx += 1
				#return StatusRecordParser(n, 28, "Riding")
				return RidingRecordParser(n)
			elif n == "223C":
				self.idx += 1
				return StatusRecordParser(n, 28)
			elif n == "232C":
				self.idx += 1
				return StatusRecordParser(n, 29)
			elif n == "2433":
				self.idx += 1
				return RegistrationParser(n)
			elif n == "2533":
				self.idx += 1
				return StatusRecordParser(n, 31)
			else:
				if FlagParser().matches(c):
					self.idx -= 1
					return FlagParser()

	def parse(self, byte):
		c = hexOfByte(byte)

		if self.activeParser:
			r = self.activeParser.parse(byte)
			if r != None:
				self.out(str(self.parserIndex) + ": " + r + "\n")
				self.activeParser = None
		else:
			self.parserIndex = self.idx
			self.activeParser = self.getMatchingParser(c)
			if not self.activeParser:
				try:
					if printPrintable and byte in self.printable:
						self.out(byte)
					else:
						self.out("<" + c + ">")

					if c == "B2":
						self.out('\n')
				except:
					print self.idx
printPrintable = False

if __name__ == "__main__":
	import argparse
	import sys

	parser = argparse.ArgumentParser()
	parser.add_argument("logfile")
	parser.add_argument("-o", "--output-file", type=str, help="file to write output to (defaults to stdout)")
	parser.add_argument("-d", "--dump-records", action="store_true", help="dump the bytes in raw-ish format after splitting on record boundaries and translating to hexadecimal")
	parser.add_argument("-t", "--run-tests", action="store_true", help="run some tests on the script parser.")
	args = parser.parse_args()

	if args.output_file:
		out = file(args.output_file, "w")
	else:
		out = sys.stdout

	f = MasterParser(args.logfile, out)

	if args.dump_records:
		f.dumpRecordWidthLine()
		f.dumpRecords()
	elif args.run_tests:
		bytes = map(chr, [0X07,0X00,0X06,0X0C,0X0A,0X0B,0X00,0xB2])
		f.out("findValidSubRecordLength('" + hexOfBytes(bytes) + "') = " + str(f.findValidSubRecordLength(bytes))+"\n")

		bytes = map(chr, [0X07,0X00,0X07,0X0C,0X0A,0X0B,0X00,0xB2])
		f.out("findValidSubRecordLength('" + hexOfBytes(bytes) + "') = " + str(f.findValidSubRecordLength(bytes)) + "\n")
		
	else:
		f.parseFile()


