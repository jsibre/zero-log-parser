import inspect
import struct
import sys
import math
import traceback

from utils import *

class BaseParser(object):
	recordTypeByte = None
	struct = None

	structPatternPrefix = "=bbi"
	structKeysPrefix = "length,type,timestamp".split(",")

	structPattern = ""
	structKeys = []

	structPatternSuffix = "x"
	structKeysSuffix = []

	parseFailureMessage = None
	timestamp = None

	label = "BaseParser"
	debug = False

	@classmethod
	def matches(cls, record):
		# Generic matcher - may be overridden for more complicated matching algorithms
		try:
			if cls.recordTypeByte == record.data[1]:
				return True
		except Exception as e:
			sys.stderr.write(str(e))
			return False

	def __init__(self, record):
		self.record = record

		try:
			self.fixup()	# Give the subclass a chance to make any tweaks...
		except NotImplementedError as e:
			# if the subclass needs to punt, it will throw NotImplementedError
			self.parseFailureMessage = str(e)
			return

		self.struct = struct.Struct(self.getStructPattern())

		if self.__class__ == BaseParser.__class__:
			self.parseFailureMessage = "NO PARSER MATCHED"
			return
		elif struct.calcsize(self.struct.format) != record.length:
			self.parseFailureMessage = "Struct size of %s bytes does not match record length of %s bytes. (struct = \"%s\")" % (struct.calcsize(self.struct.format), record.length, self.struct.format )
			return

		try:				
			self.parsePrefix()
			self.parse()
			self.parseSuffix()
		except Exception as e:
			self.parseFailureMessage = "PARSE ERROR: Exception while parsing record:" + str(e)

	def getStructPattern(self):
		return self.structPatternPrefix + self.structPattern + self.structPatternSuffix

	def getStructKeys(self):
		return self.structKeysPrefix + self.structKeys + self.structKeysSuffix

	def parsePrefix(self):
		self.unpack()
		self.indicatedLength = self.getStructValue('length')
		self.recordTypeByteActual = self.getStructValue('type')

		self.timestamp = convertIntToTime(self.getStructValue('timestamp'))

	def fixup(self):
		# here to be overridden
		pass

	def parse(self):
		# Here to be overridden
		pass

	def parseSuffix(self):
		# Not needed at this time. Maybe later.
		pass

	def getTimestamp(self):
		if self.timestamp:
			return self.timestamp
		elif self.timestamp == 0:
			return 0
		else:  # try to guess
			#Typically, the timestamp is in bytes 3,4,5,6, but sometimes it's in 4,5,6,7.
			# Only way I know how to tell so far is to try the first option, and if it doesn't 
			# look sane, try the second, and see if it does.

			for idx in (2,3):
				ts = self.getSaneTimestamp(idx)
				if ts:
					return ts

	def getSaneTimestamp(self, index):
		structPattern = struct.Struct("i")
		if index+4 <= len(self.record.data):
			ts = structPattern.unpack(self.record.data[index:index+4])[0]
			if ts > 1200000000 and ts < time.time() + 100000000:
				return convertIntToTime(ts)


	def getFormattedTimestamp(self, guess=False):
		ts = None
		if self.timestamp:
			ts = self.timestamp
		else:
			if guess:
				ts = self.getTimestamp()

		if ts:
			return time.strftime("%Y-%m-%d %H:%M:%S", ts)
		else:
			return " - - - - - - - - - "

	def decoratedOutputBuffer(self):
		if not self.struct:
			return self.record.toHex()
		else:
			start = 0
			output = ""
			ob = self.record.toHex()
			fieldWidth = ""
			for c in self.struct.format:
				d=""
				if c == "=":
					l=0
				elif c in "x":
					l=1
					d="x"
				elif c in "Bbc?s":
					l=1
				elif c in "hH":
					l=2
				elif c in "iIlLf":
					l=4
				elif c in "dqQ":
					l=8
				elif c in "0123456789":
					l=0
					fieldWidth += c
				else:
					return "UNRECOGNIZED FIELD SPECIFIER - " + c
				
				if l != 0 and fieldWidth != "":
					l = int(fieldWidth)
					fieldWidth = ""

				if l:
					output += d + ob[start:start+(l*2)] + d
					output += " "
					start = start + (l*2)
			output += ob[start:]
			return output

	def unpack(self):
		self._result = self.struct.unpack(self.record.data)

	def getStructValue(self, key):
		return self._result[self.getStructKeys().index(key)]

	def dump(self):
		output = []
		for k in self.getStructKeys():
			output.append(k + ":" + str(self.getStructValue(k)))
		#fs = "%6s " * len(self._result)
		#return fs % self._result
		return ", ".join(output) 

	def parseResults(self):
		return "Unparsed: " + self.record.toHex()

	def getResults(self, debug = False):
		try:
			label = self.label or self.__class__.__name__
			if self.parseFailureMessage:
				return "%13s : %s %-26s %s %s %s" % (self.record.getLocation(), self.getFormattedTimestamp(True), label, "FAILED TO PARSE", self.record.toHex(), self.parseFailureMessage)
			else:

				if self.debug or debug:
					return "%13s : %s %-26s %s %s" % (self.record.getLocation(), self.getFormattedTimestamp(), label, self.decoratedOutputBuffer(), self.dump())
				else:
					return "%13s : %s %-26s %s" % (self.record.getLocation(), self.getFormattedTimestamp(), label, self.parseResults())
		except Exception as e:
			return "PARSE ERROR: Exception while rendering results (getResults) : " + str(e) + "\n" + traceback.format_exc()


class LogRecordParser(BaseParser):
	recordTypeByte = chr(0xFD)
	label = "Log"
	structPattern = "STRING"
	structKeys = "message".split(",")

	def __init__(self, record):
		stringLen = record.length - 1 - 1 - 4 - 1
		self.structPattern = '%ss' % stringLen
		super(LogRecordParser, self).__init__(record)

	def parseResults(self):
		return self.getStructValue('message').replace('\x00','')



class RidingRecordParser(BaseParser):
	recordTypeByte = chr(0x2C)
	label="Riding"
	structPattern="bbbxihhiBxBBxbxHxx"
	structKeys="packTH,packTL,packSOC,vPack,motTemp,ctrlTemp,motRPM,battAmps,mods,motAmps,ambTemp,odo".split(",")
	#debug = False

	@classmethod
	def matches(cls, record):
		try:
			if cls.recordTypeByte == record.data[1]:
				if record.data[0] == chr(0x22): # We know how to parse this record
					return True
				else:
					return True
		except Exception as e:
			sys.stderr.write(str(e))
			return False

	def fixup(self):

		if self.record.indicatedLength == 0x23:
			#Not sure about this...
			#self.structPattern="bbbxihhiBxBBxbxxHxx"
			pass
		elif self.record.indicatedLength == 0x24:
			# WTF???
			raise NotImplementedError("Found 0x2C record with indicated length of 0x24 - not sure what to do with it.")

	def parseResults(self):
		return "PackTemp: h%3sC, l%3sC, PackSOC:%3s%%, Vpack:%7.3fV, MotAmps: %3s, BattAmps: %3s, Mods: %s, MotTemp: %3sC, CtrlTemp: %3sC, AmbTemp: %3sC, MotRPM:%4s, Odo: %skm" % ( 
			self.getStructValue("packTH"),
			self.getStructValue("packTL"),
			self.getStructValue("packSOC"),
			self.getStructValue("vPack")/1000.0,
			self.getStructValue("motAmps"),
			self.getStructValue("battAmps"),
			binary(self.getStructValue("mods"), 2, True),
			self.getStructValue("motTemp"),
			self.getStructValue("ctrlTemp"),
			self.getStructValue("ambTemp"),
			self.getStructValue("motRPM"),
			self.getStructValue("odo")
			)

class DisarmedParser(BaseParser):
	# same format as RidingRecordParser - perhaps one parser should do both, and just change the label based on the RecordType
	recordTypeByte = chr(0x3C)
	label="Disarmed"
	structPattern="bbbxihhiBxBBxbxHxx"
	structKeys="packTH,packTL,packSOC,vPack,motTemp,ctrlTemp,motRPM,battAmps,mods,motAmps,ambTemp,odo".split(",")

	def parseResults(self):
		return "PackTemp: h%3sC, l%3sC, PackSOC:%3s%%, Vpack:%7.3fV, MotAmps: %3s, BattAmps: %3s, Mods: %s, MotTemp: %3sC, CtrlTemp: %3sC, AmbTemp: %3sC, MotRPM:%4s, Odo: %skm" % ( 
			self.getStructValue("packTH"),
			self.getStructValue("packTL"),
			self.getStructValue("packSOC"),
			self.getStructValue("vPack")/1000.0,
			self.getStructValue("motAmps"),
			self.getStructValue("battAmps"),
			binary(self.getStructValue("mods"), 2, True),
			self.getStructValue("motTemp"),
			self.getStructValue("ctrlTemp"),
			self.getStructValue("ambTemp"),
			self.getStructValue("motRPM"),
			self.getStructValue("odo")
			)

class EmergencyFrameRecordParser(BaseParser):
	recordTypeByte = chr(0x2F)
	#length=9
	label="SEVCON CAN EMCY Frame"
	structPattern="HhbBBB"
	structKeys="errCode,sevErrCode,errReg,d1,d2,d3".split(",")
	#debug = True

	sevConErrCodes = {
		"0x4603":"Motor in thermal c/b",
		"0x4884":"Sequence Fault",
		"0x5101":"Line Contactor o/c",
		"0x5319":"Mtr Slv in Wrong State"
	}


	def parseResults(self):
		return "Error Code: %s, Error Reg: %s, Sevcon Error Code: %s, Data: %s %s %s, %s" % (
				hexOfWidth(self.getStructValue("errCode"), 4), 
				hexOfWidth(self.getStructValue("errReg"),2), 
				hexOfWidth(self.getStructValue("sevErrCode"),4), 
				hexOfWidth(self.getStructValue("d1"),2), 
				hexOfWidth(self.getStructValue("d2"),2), 
				hexOfWidth(self.getStructValue("d3"),2),
				self.sevConErrCodes.get(hexOfWidth(self.getStructValue("sevErrCode"),4), "Unrecognized Sevcon Error Code **********")
				)

class CANLinkUpParser(BaseParser):
	recordTypeByte = chr(0x28)
	label="CAN Link Up"
	structPattern="b"
	structKeys="module".split(",")
	debug = False

	def parseResults(self):
		return "Module %02i CAN Link Up" % ( 
			self.getStructValue("module"),
			)

class CANLinkDownParser(BaseParser):
	recordTypeByte = chr(0x29)
	label="CAN Link Down"
	structPattern="b"
	structKeys="module".split(",")
	debug = False

	def parseResults(self):
		return "Module %02i CAN Link Down" % self.getStructValue("module")

class SevconPowerParser(BaseParser):
	recordTypeByte = chr(0x34)
	label="Sevcon Power"
	structPattern="bb"
	structKeys="power".split(",")
	debug = False

	def parseResults(self):
		return "Sevcon Turned %s" % self.getStructValue("power") and "On" or "Off"

class ChargingRecordParser(BaseParser):
	recordTypeByte = chr(0x2D)
	label="Charging"
	structPattern="bbbxihhbbb"
	structKeys="packTH,packTL,packSOC,packV,battAmps,MbbChgEn,Mods,AmbTemp,BmsChgEn".split(",")
	debug = False

	@classmethod
	def matches(cls, record):
		try:
			if cls.recordTypeByte == record.data[1]:
				if record.data[0] == chr(0x16): # We know how to parse this record
					return True
				else:
					return True
		except Exception as e:
			sys.stderr.write(str(e))
			return False

	def parseResults(self):
		return "PackTemp: h%3sC, l%3sC, AmbTemp: %sC, PackSOC:%3s%%, Vpack:%6.3fV, BattAmps: %3s, Mods: %s, MbbChgEn: %s, BmsChgEn: %s" % ( 
			self.getStructValue("packTH"),
			self.getStructValue("packTL"),
			self.getStructValue("AmbTemp"),
			self.getStructValue("packSOC"),
			self.getStructValue("packV")/1000.0,
			self.getStructValue("battAmps"),
			binary(self.getStructValue("Mods"), 2, True),
			yesOrNo(self.getStructValue("MbbChgEn")),
			yesOrNo(self.getStructValue("BmsChgEn"))
			)

class LowChassisIsolationParser(BaseParser):
	recordTypeByte = chr(0x3A)
	label="Low Chassis Isolation"
	structPattern="bxxxb"
	structKeys="isolation,cell".split(",")

	def parseResults(self):
		#return repr(self._result)
		return "%s KOhms to cell %s" % ( 
			self.getStructValue("isolation"),
			self.getStructValue("cell")
			)

class BMSIsolationFaultParser(BaseParser):
	recordTypeByte = chr(0x31)
	label="BMS Isolation Fault"
	structPattern="b"
	structKeys="pack".split(",")

	def parseResults(self):
		#return repr(self._result)
		return "BMS: %s" % ( 
			self.getStructValue("pack"),
			)

class RegistrationParser(BaseParser):
	recordTypeByte = chr(0x31)
	label="Registration"
	structPattern="xBi23s"
	structKeys="module,vmod,serial".split(",")
	debug = False

	def parseResults(self):
		return "Module %02i Registered     serial: %s,  vmod:%8.3f" % ( 
			self.getStructValue("module") ,
			self.getStructValue("serial").strip(),
			self.getStructValue("vmod")/1000.0
			)

class ModuleStateChangeParser(BaseParser):
	recordTypeByte = chr(0x33)
	label="Module Change"
	structPattern="BBiiiihhx"
	structKeys="action,module,vmod,maxsys,minsys,vcap,battCurr".split(",")
	debug=False

	actions = {
		0 : 'Opening Contactor',
		1 : 'Closing Contactor',
		3 : 'CAN Disconnect'
		}

	def parseResults(self):
		action = self.actions.get(self.getStructValue("action"),"Unknown Action!")

		label = "Module %s %s " % (
			self.getStructValue("module"),
			action
			)


		if self.getStructValue("action") == 1:
			diff = (self.getStructValue("maxsys")/1000.0) - (self.getStructValue("minsys")/1000.0)
			prechg = math.floor((self.getStructValue("vcap")/1000.0) / (self.getStructValue("vmod")/1000.0) * 100)
			return "%s vmod:%8.3fV, maxsys:%8.3fV, minsys:%8.3fV, diff:%6.3fV, vcap:%8.3fV, prechg: %i%%" % ( 
				label,
				self.getStructValue("vmod")/1000.0,
				self.getStructValue("maxsys")/1000.0,
				self.getStructValue("minsys")/1000.0,
				diff,
				self.getStructValue("vcap")/1000.0,
				prechg
				)
		elif self.getStructValue("action") == 0:
			return "%s vmod:%8.3fV, batt curr: %4sA" % ( 
				label,
				self.getStructValue("vmod")/1000.0,
				self.getStructValue("battCurr")
				)
		elif self.getStructValue("action") == 3:
			return "%s vmod:%8.3fV" % ( 
				label,
				self.getStructValue("vmod")/1000.0
				)

class SevconCanUpParser(BaseParser):
	recordTypeByte = chr(0x2A)
	label="Sevcon CAN"
	structPattern=""
	structKeys=[]

	def parseResults(self):
		return "Sevcon CAN Link Up"

class SevconCanDownParser(BaseParser):
	recordTypeByte = chr(0x2B)
	label="Sevcon CAN"
	structPattern=""
	structKeys=[]

	def parseResults(self):
		return "Sevcon CAN Link Up"

class ChargerEventParser(BaseParser):
	recordTypeByte = chr(0x30)
	label="Charger Event"
	structPattern="bb"
	structKeys="charger,connected".split(",")
	
	def parseResults(self):
		if self.getStructValue("connected"):
			event = "Connected"
		else:
			event = "Disconnected"

		return "Calex 1200W Charger %s %s" % ( 
				self.getStructValue("charger")-1,
				event
				)

class MotorControllerHighParser(BaseParser):
	# Temperature warning.  Third value not used.
	recordTypeByte = chr(0x26)
	label="High Mot/Ctrl"
	structPattern="HHxx"
	structKeys="mot,ctrl".split(",")

	def parseResults(self):
		return "Mot: %sC, Ctrl: %sC" % ( 
				self.getStructValue("mot"),
				self.getStructValue("ctrl")
				)

class BoardResetParser(BaseParser):
	recordTypeByte = chr(0x01)
	label="Board Reset"
	structPattern="b"
	structKeys="value".split(",")

	def parseResults(self):
		if self.getStructValue("value") == 4:
			return "Software"
		else:
			return "Unexpected value (%s)" % self.getStructValue("value")

class KeyChangeParser(BaseParser):
	recordTypeByte = chr(0x09)
	label="Key Change"
	structPattern="b"
	structKeys="on".split(",")

	def parseResults(self):
		if self.getStructValue("on"):
			return "Key On"
		else:
			return "Key Off"

class SevconPowerParser(BaseParser):
	recordTypeByte = chr(0x36)
	label="Sevcon Power"
	structPattern="b"
	structKeys="on".split(",")

	def parseResults(self):
		if self.getStructValue("on"):
			return "Sevcon Turned On"
		else:
			return "Sevcon Turned Off"

class BatteryDischargeLimitedParser(BaseParser):
	recordTypeByte = chr(0x39)
	label="Batt Dischg Cur Limited"
	structPattern="HHbbh"
	structKeys="amps,minCellMilli,maxPackTemp,value4,v5".split(",")

	def parseResults(self):
		return "%s A (%2.0f%%), MinCell: %smV, MaxPackTemp: %sC" % (
			self.getStructValue("amps"),
			self.getStructValue("amps")/695.0*100,
			self.getStructValue("minCellMilli"),
			self.getStructValue("maxPackTemp")
			)


class SevconPrechargeParser(BaseParser):
	recordTypeByte = chr(0x3D)
	label="Sevcon Precharge"
	structPattern="i"
	structKeys="millivolts".split(",")

	def parseResults(self):
		return "Sevcon Failed To Fully Precharge (%s mV). Restarting Sevcon." % (self.getStructValue('millivolts'))

class MainPowerParser(BaseParser):
	recordTypeByte = chr(0x34)
	label=""
	structPattern="bb"
	structKeys="powerOn,source".split(",")

	sources = {
		1 : 'Key Switch',
		4 : 'Onboard Charger'
		}

	def parseResults(self):
		return "Power %3s %25s" % (
			onOrOff(self.getStructValue("powerOn")),
			self.sources.get(self.getStructValue("source"), 'Unrecognized Source (%s)' % self.getStructValue("source"))
		)

#####################################################################################
#####################################################################################
#####################################################################################



def getMatchingParser(record):
	for n,p in parsers:
		if p.matches(record):
			return p

	# BaseParser will function as wildcard parser.
	# It won't match (above), but it can handle anything, in a generic way
	return BaseParser


# Get a list of classes (Parsers) defined in this module.
parsers = inspect.getmembers(sys.modules[__name__], lambda member: inspect.isclass(member) and member.__module__ == __name__ and issubclass(member, BaseParser))

for n,p in parsers:
	if p.recordTypeByte:
		print hex(ord(p.recordTypeByte)), 
	else:
		print None,
	print n
