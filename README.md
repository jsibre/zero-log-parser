# Zero Log Parser

This is a small python utilty that parses the *.bin files that can be retrieved via the 
iPhone/Android apps for Zero Motorcycles.

This log file seems to be a subset of the bike's actual logs - it's much smaller, for one thing.

## Installation

No real installation required.  Just run the zerologparser.py file, providing your *.bin file as an argument:

	zerologparser.py 538SM7Z33FCGxxxxx_MBB_2015-07-30.bin

Output is directed to stdout.  You can redirect it to a file using standard shell semantics:

	zerologparser.py 538SM7Z33FCGxxxxx_MBB_2015-07-30.bin > readable.log

There are various options that may be passed as well.  These are in flux - run the command with no arguments, or with "-h" to see the current flags and what they do.

## Notes

The file consists of five logical areas that I'm aware of.  I think of them as headers 1-4 and the body.

Header 4 consists of records - error records worth storing, I think, and the body consists of records.

Records are terminated by a 0xB2.  Textual log entries are NUL terminated (0x00) C-style strings, which are then followed by a 0xB2.

The general format of a record is:

	<record length byte><record type byte><integer timestamp><data>...<data><record terminator>

For example, a text log entry may look like this (each pair is a hexadecimal representation of a byte):

	29FD8E209D5544454255473A20536576636F6E20436F6E746163746F72204472697665204F4E2E00B2

which breaks down like so

	29 - record is 0x29 bytes long
	FD - it's a textual log entry
	8E209D55 - 2015-07-08 08:07:26
	44454255473A20536576636F6E20436F6E746163746F72204472697665204F4E2E00 - "DEBUG: Sevcon Contactor Drive ON.<NUL>"
	B2 - end of record

## Remaining Mysteries

Timezone to decode timestamps to?  It's seconds since epoch (01-01-1970 00:00:00), but I'm not sure if that's based on the time set on the bike (localtime, in all likelihood), or if it's some internal clock that can't be touched (Pacific Time, most likely).  A bit more observation of real data should make it clear.  The thing that made me notice the problem is that some records have a timestamp of "0", which, if treated as GMT and shifted to my timezone, works out as Dec 31, 1969, 18:00:00.

For now, the script assumes "0" is "no time", and prints " - - - - - - - ".  As for whether to decode to local time or GMT?  A bit of observation will straighten that out.

I occasionally find records of a particular type that have an extra byte or two in them.  Very confusing.  

Also related (perhaps), some records seem to have an extra byte BEFORE the timestamp. This causes the timestamp decode to go wonky for that record.  It's obvious enough that the timestamp data has shifted one byte to the right, but it's not apparent how to recognize a record that has happened to before looking at the record to try and spot the four bytes for the timestamp. 

Perhaps those records have a two byte record type identifier? 

Here are some samples of what I think are "Riding" records:

	(white space added to illustrate some of the groups)
	22 2C   C33A8355 22214200E5C7010029001E00BE0000000000000000  20008019 0000 B2
	23 2C   FF3A8355 22214200E5C701001E001E00FE01010000000000000020008119 0000 B2
	24 2C11 FE018B55 27234F00D1A801004D002500FE0105000014000334001F008D1A 0000 B2

Note that the top two look very similar in structure, but one has an extra NUL byte in the middle-ish. 
The third is plainly a completely different type of record despite having that 2C in it.  I'll tell ya... this really makes me wonder if I'm completely wrong about the second byte being a record type indicator.


